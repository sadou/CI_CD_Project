package domain;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.geotools.feature.SchemaException;
import org.junit.BeforeClass;
import org.junit.Test;

import ensg.tsi.tsdiakhaby.shapewritter.ShapeReader;

public class ShapeReaderTest 
{
	private static ShapeReader reader;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		reader = new ShapeReader("shapefiles/output/", "test");
		reader.read();
	}
	@Test
	public void testRead() throws SchemaException, IOException 
	{
		assertTrue(null!=reader.read());
	}

	@Test
	public void testClose() throws IOException 
	{
		reader.close();
		assertTrue(reader.isClosed()==true);
	}

}
