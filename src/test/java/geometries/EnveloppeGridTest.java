package geometries;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ensg.tsi.tsdiakhaby.geometries.Enveloppe;
import ensg.tsi.tsdiakhaby.geometries.EnveloppeGrid;

public class EnveloppeGridTest {

	@Test
	public void testBuildEnveloppe() 
	{
		Enveloppe[][] envGrid =EnveloppeGrid.buildGridEnveloppe(13.5,12.5,42.5,43.6, 10.0d);
		
		assertTrue((13.5==envGrid[0][0].getXmin()) && (12.5==envGrid[0][0].getYmin()) );	
		assertTrue(0<envGrid[envGrid.length-1][envGrid[1].length-1].getXmax() && 0<envGrid[envGrid.length-1][envGrid[1].length-1].getYmax());
	}

}
