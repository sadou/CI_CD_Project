package geometries;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ensg.tsi.tsdiakhaby.geometries.Coordinate;
import ensg.tsi.tsdiakhaby.geometries.Enveloppe;
import ensg.tsi.tsdiakhaby.geometries.Point;

public class EnveloppeTest 
	{
		static Enveloppe env;
	
		@BeforeClass
		public static void setUpBeforeClass() throws Exception
		{
			Coordinate c1 = Mockito.mock(Coordinate.class);
			Coordinate c2 = Mockito.mock(Coordinate.class);
			
			Mockito.when(c1.getX()).thenReturn(13.5);
			Mockito.when(c1.getY()).thenReturn(12.5);
			
			Mockito.when(c2.getX()).thenReturn(42.5);
			Mockito.when(c2.getY()).thenReturn(43.6);
			
			env = new Enveloppe(c1,c2);
		}
	
		@Test
		public void testGetXmin() 
		{
			assertTrue(13.5==env.getXmin());
		}
	
		@Test
		public void testGetYmin() {
			assertTrue(12.5==env.getYmin());
		}
	
		@Test
		public void testGetXmax() {
			assertTrue(42.5==env.getXmax());
		}
	
		@Test
		public void testGetYmax() 
		{
			assertTrue(43.6==env.getYmax());
		}
		@Test
		public void testContains()
		{
			Point point = Mockito.mock(Point.class);
			
			Mockito.when(point.getX()).thenReturn(22.05);
			Mockito.when(point.getY()).thenReturn(22.05);
			
			assertTrue(env.contains(point));
		}

}
