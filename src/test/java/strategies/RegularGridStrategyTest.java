package strategies;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import ensg.tsi.tsdiakhaby.geometries.Geometry;
import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.strategies.RegularGridStrategy;
import ensg.tsi.tsdiakhaby.strategies.StrategyFactory;

public class RegularGridStrategyTest {
	static StrategyFactory factory;
	static List<Point> listPoint; 

	@BeforeClass
	public static void setUpBeforeClass() throws Exception 
	{
		Point point1 = Mockito.mock(Point.class);
		Point point2 = Mockito.mock(Point.class);
		Point point3 = Mockito.mock(Point.class);
		Point point4 = Mockito.mock(Point.class);
		Point point5 = Mockito.mock(Point.class);
		Point pMin   = Mockito.mock(Point.class);
		Point pMax   = Mockito.mock(Point.class);
		
		Mockito.when(point1.getX()).thenReturn(1.8);
		Mockito.when(point1.getY()).thenReturn(1.0);
		
		Mockito.when(point2.getX()).thenReturn(2.0);
		Mockito.when(point2.getY()).thenReturn(2.0);
		
		Mockito.when(point3.getX()).thenReturn(3.0);
		Mockito.when(point3.getY()).thenReturn(3.0);
		
		Mockito.when(point4.getX()).thenReturn(8.0);
		Mockito.when(point4.getY()).thenReturn(1.0);
		
		Mockito.when(point5.getX()).thenReturn(11.0);
		Mockito.when(point5.getY()).thenReturn(20.5);
		
		Mockito.when(pMin.getX()).thenReturn(0.0);
		Mockito.when(pMin.getY()).thenReturn(0.0);

		Mockito.when(pMax.getX()).thenReturn(100.0);
		Mockito.when(pMax.getY()).thenReturn(100.0);
		
		listPoint = new ArrayList<Point>();
		listPoint.add(point1);
		listPoint.add(point2);
		listPoint.add(point3);
		listPoint.add(point4);
		listPoint.add(point5);
		listPoint.add(pMin);
		listPoint.add(pMax);
		factory = Mockito.mock(StrategyFactory.class);
		Mockito.when(factory.createStrategy("regularGrid")).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                return new RegularGridStrategy();
            }
        });
	}

//	@Test
//	public void testRegularGrid() 
//	{
//		double tolerance = 10;
//		IGeneralizatorStrategy strategy = (RegularGridStrategy) factory.createStrategy("regularGrid");
//		List<? extends Geometry> remainingPoint = strategy.regularGrid(listPoint.subList(0, listPoint.size()-2), listPoint.get(listPoint.size()-2), listPoint.get(listPoint.size()-1), tolerance);
//		assertTrue(remainingPoint.size()<=listPoint.size());
//		
//	}

	@Test
	public void testGeneralize() {
		double tolerance = 10;
	    List<? extends Geometry> remainingPoint = factory.createStrategy("regularGrid").generalize(listPoint, tolerance);
		assertTrue(remainingPoint.size()<=listPoint.size());
	}

}
