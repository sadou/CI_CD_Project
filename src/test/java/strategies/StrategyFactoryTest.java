package strategies;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ensg.tsi.tsdiakhaby.exceptions.UnknownStrategyException;
import ensg.tsi.tsdiakhaby.strategies.DouglasPeuckerStrategy;
import ensg.tsi.tsdiakhaby.strategies.IGeneralizatorStrategy;
import ensg.tsi.tsdiakhaby.strategies.RegularGridStrategy;
import ensg.tsi.tsdiakhaby.strategies.StrategyFactory;

public class StrategyFactoryTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testCreateStrategy() 
	{
		StrategyFactory factory = new StrategyFactory();
		IGeneralizatorStrategy douglasStrategy = factory.createStrategy("douglasPeucker");
		IGeneralizatorStrategy regularGridStrategy = factory.createStrategy("regularGrid");
	    
		assertTrue(douglasStrategy instanceof DouglasPeuckerStrategy);
		assertTrue(regularGridStrategy instanceof RegularGridStrategy);
	}
	
	@Test(expected = UnknownStrategyException.class)
	public void uknownMethodTest() {
		StrategyFactory factory = new StrategyFactory();
		factory.createStrategy("regul");
	}


}
