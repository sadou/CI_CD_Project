package ensg.tsi.tsdiakhaby.geometries;

/**
 * Classe permettant de créer un objet de coordonnées d'une abscisse et d'une ordonnée
 * @author tsdiakhaby
 * @version 1.1
 */

public class Coordinate 
{
	private  com.vividsolutions.jts.geom.Coordinate coordinate;
	
	public Coordinate ()
	{
		this.coordinate = new com.vividsolutions.jts.geom.Coordinate();
	}
	
	public Coordinate (double x, double y)
	{
		this.coordinate = new com.vividsolutions.jts.geom.Coordinate(x,y);
	}
	
	public double getX()
	{
		return this.coordinate.x;
	}
	public double getY()
	{
		return this.coordinate.y;
	}
}
