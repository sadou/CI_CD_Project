package ensg.tsi.tsdiakhaby.geometries;


public interface Geometry
{
	public String getType();
	public Coordinate[] getCoordinate();
}
