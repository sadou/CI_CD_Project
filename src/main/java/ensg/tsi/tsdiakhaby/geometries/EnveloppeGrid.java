package ensg.tsi.tsdiakhaby.geometries;

/**
 * Classe permettant de créer une grille régulière d'enveloppe
 * @author tsdiakhaby
 * @version 1.1
 */
public class EnveloppeGrid 
{
	/**
	 * Construit une grille régulière
	 * @param xMin   double l'abscisse du bord inférieur de l'emprise des données
	 * @param yMin   double l'ordonnée du bord inférieur de l'emprise des données
	 * @param xMax   double l'abscisse du bord supérieur de l'emprise des données
	 * @param yMax   double l'ordonnée du bord supérieur de l'emprise des données
	 * @param pasGrille double pas  de la grille régulière
	 * @return une matrice contenant les bords d'une enveloppe (bord inférieur et surpérieur)
	 */
	public static Enveloppe[][] buildGridEnveloppe(double xMin, double yMin, double xMax, double yMax,double pasGrille)
	{
		//Calcul de la taille de la grille 
	    int sizeX = (int) ((xMax - xMin)/pasGrille);
	    int sizeY = (int) ((yMax - yMin)/pasGrille);

        Enveloppe[][] grilleEnv = new Enveloppe[sizeX+1][sizeY+1];
       
        double yMin0=yMin;
        double xMin0=xMin;
       
        for(int i =0; i<sizeX+1; i++)
        {
        	yMin0 = yMin;
        	for(int j = 0; j<sizeY+1 ; j++)
        	{
        		Enveloppe env = new Enveloppe(new Coordinate(xMin0, yMin0),new Coordinate(xMin0+pasGrille, yMin0+pasGrille));
        		grilleEnv[i][j]  = env;
        		yMin0 += pasGrille;
        	}
        	xMin0 +=pasGrille;
        }
       return grilleEnv;
	}
	
}



