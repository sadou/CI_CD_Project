package ensg.tsi.tsdiakhaby.geometries;
/**
 * Classe permettant de créer une enveloppe
 * @author tsdiakhaby
 * @version 1.1
 */
public class Enveloppe {
		
		//Coordonnée du point inférieur de l'enveloppe
		private Coordinate bottomLeft ;
		
		//Coordonnée du point supérieur de l'enveloppe
		private Coordinate topRight ;
		
		//Construction de l'enveloppe
		public Enveloppe(){
			this.bottomLeft = new Coordinate();
			this.topRight = new Coordinate();	
		}
		
		/**
		 * Construit une enveloppe à partir des coordonnées de deux points 
		 * @param bottomLeft Coordinate Coordonnée du point inférieur de l'enveloppe
		 * @param topRight Coordinate Coordonnée du point inférieur de l'enveloppe
		 */
		public Enveloppe(Coordinate bottomLeft, Coordinate topRight){
			this.bottomLeft = bottomLeft;
			this.topRight = topRight;	
		}

		/**
		 * Retroune la valeur de l'abscisse du point inférieur de l'envellope
		 * @return double
		 */
		public double getXmin(){
			return bottomLeft.getX();
		}
		
		/**
		 * Retroune la valeur de l'ordonnée du point inférieur de l'envellope
		 * @return double
		 */
		public double getYmin(){
			return bottomLeft.getY();
		}
		
		/**
		 * Retroune la valeur de l'abscisse du point supérieur de l'envellope
		 * @return double
		 */
		public double getXmax(){
			return topRight.getX();
		}
		
		/**
		 * Retroune la valeur de l'ordonnée du point supérieur de l'envellope
		 * @return double
		 */
		public double getYmax(){
			return topRight.getY();
		}
		
		/**
		 * teste si un point appartient à l'enveloppe
		 * Dans notre cas les enveloppes sont rectangulaires
		 * @return double
		 */
		public boolean contains(Point ptCheck) 
		{
			return (this.getXmin()<=ptCheck.getX()) && (ptCheck.getX()<=this.getXmax()) && (this.getYmin()<=ptCheck.getY()) && (ptCheck.getY()<=this.getYmax());
	    }
		
	
}
