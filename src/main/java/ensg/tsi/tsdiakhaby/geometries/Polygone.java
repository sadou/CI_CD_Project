package ensg.tsi.tsdiakhaby.geometries;

/**
 * Classe créant un polygone
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;

/**
 * Classe permettant de créer un polygone à partir des coordonnées d'un ensemble de coordonnées
 * @author tsdiakhaby
 * @version 1.1
 */

import java.util.List;

import ensg.tsi.tsdiakhaby.exceptions.NoEnoughPointsForPolygon;

public class Polygone extends AbstractGeometry 
{
	//Liste des points constituant le polygone
	private List<Point> listPoints = new ArrayList<Point>();
	
	/**
	 * Construit un polygone vide
	 */
	public Polygone()
	{}
	
	/**
	 * Construit un polygone à travers une liste de coordonnées
	 * Si le dernier point est différent du premier alors, le premier est ajouté à la fin
	 * @param  lsPoint List<Coordinate> la liste des points pour construire la ligne
	 */
	public Polygone(List<Point> lsPoint)
	{
		if(lsPoint.size()==2)
		{
			throw new NoEnoughPointsForPolygon(2);
		}
		
		this.listPoints.addAll(lsPoint);
		
		if(!lsPoint.get(0).equals(lsPoint.get(lsPoint.size()-1)))
		{
			this.listPoints.add(lsPoint.get(0));
		}
	}
	

	@Override
	public String getType() {
		return "Polygone";
	}

	/**
	 * Renvoie l'ensemble des coordonnées de la ligne
	 * @return  Coordinate[] le tableau des coordonnées.
	 */
	@Override
	public Coordinate[] getCoordinate() {
		Coordinate[] tabCoords = new Coordinate[this.listPoints.size()];
        for(int i = 0; i<this.listPoints.size();i++)
        {
        	tabCoords[i] = new Coordinate(this.listPoints.get(i).getX(),this.listPoints.get(i).getY());
        }
        return tabCoords;
	}
	
	/**
	 * Renvoie l'ensemble des points du polygon
	 * @return int la taille de la liste des points.
	 */
	public int getNbPoints() {
		return this.listPoints.size();
	}

}
