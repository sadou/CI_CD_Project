package ensg.tsi.tsdiakhaby.geometries;

import java.util.List;

public class GeometryFactory 
{
	/**
	 * Construit un point 
	 * @param x
	 * @param y
	 * @return Point
	 */
	public Geometry point(double x, double y)
	{
		return new Point(x,y);
	}
	
	/**
	 * Construit un point à partir d'un objet coordonné
	 * @param coord Coordinate
	 * @return Point
	 */
	public Geometry point(Coordinate coord)
	{
		return new Point(coord);
	}
	
	/**
	 * Contruit une ligne à partir d'une liste de points
	 * @param lsPoints
	 * @return Line
	 */
	public Geometry line(List<Point> lsPoints)
	{
		return new Line(lsPoints);
	}
	
	/**
	 * Contruit une ligne à partir d'un tableau de points
	 * @param lsPoints
	 * @return Line
	 */
	public Geometry line(Point[] lsPoints)
	{
		return new Line(lsPoints);
	}
	/**
	 * Contruit une ligne ne contenant aucun point
	 * @return Line
	 */
	public Geometry line()
	{
		return new Line();
	}
	
	/**
	 * Contruit un polygone à partir d'une liste de points
	 * @param lsPoints
	 * @return Polygone
	 */
	public Geometry polygone(List<Point> lsPoints)
	{
		return new Polygone(lsPoints);
	}
	
	/**
	 * Contruit un polygone vide
	 * @return Polygone
	 */
	public Geometry polygone()
	{
		return new Polygone();
	}
	
}
