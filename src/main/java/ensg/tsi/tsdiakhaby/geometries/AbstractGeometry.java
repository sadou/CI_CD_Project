package ensg.tsi.tsdiakhaby.geometries;

/**
 * Classe abstraite regroupant des fonctionnalités des géométries
 * @author tsdiakhaby
 * @version 1.1
 */
public abstract class AbstractGeometry implements Geometry
{
	protected String type;
	
	public String getType()
	{
		return this.type;
	}
}
