package ensg.tsi.tsdiakhaby.exceptions;

/**
 * Classe permettant de gérer les exceptions sur la construction des polygons avec moins de 3 points. 
 * @author tsdiakhaby
 * @version 1.1
 */
public class NoEnoughPointsForPolygon extends RuntimeException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int nbPointsGiven ;
	public NoEnoughPointsForPolygon(int nbPoints)
	{
		this.nbPointsGiven= nbPoints;
	}
	public void printStackTrace()
	{
		System.out.println("Must have at least 3 points but given :"+this.nbPointsGiven);
	}
}
