package ensg.tsi.tsdiakhaby.exceptions;
/**
 * Classe permettant de gérer les exceptions sur les tolerances négatives ou null
 * @author tsdiakhaby
 * @version 1.1
 */
public class NegativeOrNullToleranceException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private double givenTolerance ;
	
	public NegativeOrNullToleranceException(double toleranceGiven)
	{
		this.givenTolerance= toleranceGiven;
	}
	public void printStackTrace()
	{
		System.out.println("Must have at least 3 points but given :"+this.givenTolerance);
	}
}
