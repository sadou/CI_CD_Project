package ensg.tsi.tsdiakhaby.exceptions;
/**
 * Classe permettant de gérer les exceptions sur les types de stratégies inexistantes
 * @author tsdiakhaby
 * @version 1.1
 */
public class UnknownStrategyException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String strategyNameGiven; 
	public UnknownStrategyException(String strategyName) {
		this.strategyNameGiven = strategyName;
	}
	
	public void printStackTrace()
	{
		System.out.println("Strategy not found "+this.strategyNameGiven);
	}

}
