package ensg.tsi.tsdiakhaby.exceptions;

import ensg.tsi.tsdiakhaby.domain.GeometryType;

public class UnknownGeometryWritterException extends RuntimeException 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private GeometryType type; 
	public UnknownGeometryWritterException(GeometryType type) {
		this.type = type;
	}
	
	public void printStackTrace()
	{
		System.out.println("Strategy not found "+this.type);
	}

}
