package ensg.tsi.tsdiakhaby.domain;

public enum GeometryType 
{
	Point,
	LineString,
	Polygon
}
