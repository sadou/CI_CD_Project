package ensg.tsi.tsdiakhaby.strategies;
import ensg.tsi.tsdiakhaby.exceptions.UnknownStrategyException;

public class StrategyFactory 
{
	//Créateur de stratégie
	public IGeneralizatorStrategy createStrategy(String strategyName)
	{
		if(strategyName=="douglasPeucker") return new DouglasPeuckerStrategy();
		else if(strategyName=="regularGrid") return new RegularGridStrategy();
		else if(strategyName=="rightAnglePolygon") return new RightAnglePolygonStrategy();
		//Si le nom de la stratégie, on lève une exception
		else throw new UnknownStrategyException(strategyName);
	}
}
