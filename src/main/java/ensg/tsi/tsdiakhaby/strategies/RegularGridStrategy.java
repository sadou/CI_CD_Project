package ensg.tsi.tsdiakhaby.strategies;

/**
 * Classe permettant de définir la stratégie de la grille régulière
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

import ensg.tsi.tsdiakhaby.exceptions.NegativeOrNullToleranceException;
import ensg.tsi.tsdiakhaby.geometries.Enveloppe;
import ensg.tsi.tsdiakhaby.geometries.EnveloppeGrid;
import ensg.tsi.tsdiakhaby.geometries.GeometryFactory;
import ensg.tsi.tsdiakhaby.geometries.Point;

public class RegularGridStrategy implements IGeneralizatorStrategy
{
	/**
     * Simplifie une liste de points en un nombre réduit de points connaissant l'emprise des points
     * @param listPoints le nuage de points 
     * @param tolerance double distance à partir de laquelle on supprime un point du nuage
     * @return List<Point> une liste de points contenant les points moyens dans chaque cellule de la grille
     */
	public List<Point> regularGrid(List<Point> listPoints, double tolerance)
	{
       if(tolerance<=0)
		{
			throw new NegativeOrNullToleranceException(tolerance);
		}
       
       GeometryFactory geomFactory = new GeometryFactory();
       
       //On calcule l'emprise du nuage
       List<Point> bounds = getBounds(listPoints);
       Point pMin = bounds.get(0);
       Point pMax = bounds.get(1);
      //Création d'une grille régulière basée sur l'emprise du nuage de points
        Enveloppe[][] grilleEnv = EnveloppeGrid.buildGridEnveloppe(pMin.getX(),pMin.getY(),pMax.getX(),pMax.getY(), tolerance);
        
        List<Point> simplifiedPoints = new ArrayList<Point>();
        
      //Pour chaque cellule de la grille, on cherche les points du nuage qui y sont inclus 
        for(int i=0;i<grilleEnv.length;i++)
        	{
        	   for(int j=0; j<grilleEnv[1].length;j++)
        		{
        			List<Point> cellulePoints = new ArrayList<Point>();
        			for(Point readPt : listPoints)
        			{
        				if(grilleEnv[i][j].contains(readPt))
        				{
        					cellulePoints.add(readPt);
        				}	
        		    }
        			
        			//Pour l'ensemble des points appartenant à la cellule on calcule le point moyen 
        			//et on l'ajoute à la liste des points moyens
	        		if(!cellulePoints.isEmpty())
	        		  {
	        			double xMoyen = 0, yMoyen = 0;
	          			for (Point point : cellulePoints) 
	          			{
	  						 xMoyen+= point.getX();
	  						 yMoyen+= point.getY();	
	        		    }
	          			simplifiedPoints.add((Point) geomFactory.point(xMoyen/cellulePoints.size(),yMoyen/cellulePoints.size()));
	        		  }	
        	  }
          } 
       return simplifiedPoints;
	}

	private List<Point> getBounds(List<Point> listPoints) 
	{
		com.vividsolutions.jts.geom.Coordinate[] coordsIn = new com.vividsolutions.jts.geom.Coordinate[listPoints.size()+1];
		int i;
		for (i = 0; i< listPoints.size(); i++) 
		{
			coordsIn[i] = new com.vividsolutions.jts.geom.Coordinate(listPoints.get(i).getX(), listPoints.get(i).getY());
		}
		
		coordsIn[i] = new com.vividsolutions.jts.geom.Coordinate(listPoints.get(0).getX(), listPoints.get(0).getY());		
		com.vividsolutions.jts.geom.GeometryFactory fact = JTSFactoryFinder.getGeometryFactory();
        LinearRing linear = new com.vividsolutions.jts.geom.GeometryFactory().createLinearRing(coordsIn);
		Polygon poly = new Polygon(linear, null, fact);
		
		Envelope env = poly.getEnvelopeInternal();
		GeometryFactory geomFact = new GeometryFactory();
		Point pMin = (Point) geomFact.point(env.getMinX(), env.getMinY());
		Point pMax = (Point) geomFact.point(env.getMaxX(), env.getMaxY());
	    
		return Arrays.asList(pMin, pMax);
	}

	/**
     * Simplifie une liste de points en un nombre réduit de points connaissant l'emprise des points 
     * Cette méthode fait appel à la {@link #regularGrid(List<Point>,Point,Point , double) regularGrid} method.
     * @param listPoints le nuage de points 
     * @param tolerance double distance à partir de laquelle on supprime un point du nuage
     * @return List<Point> une liste de points contenant les points moyens dans chaque cellule de la grille
     */
	@Override
	public List<Point> generalize(List<Point> listPoints,double tolerance) 
	{
		return regularGrid(listPoints, tolerance);
	}
}
