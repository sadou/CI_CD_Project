package ensg.tsi.tsdiakhaby.strategies;
import java.util.Arrays;
import java.util.List;

import org.geotools.geometry.jts.JTSFactoryFinder;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

import ensg.tsi.tsdiakhaby.geometries.GeometryFactory;
import ensg.tsi.tsdiakhaby.geometries.Point;
import ensg.tsi.tsdiakhaby.geometries.Polygone;

public class RightAnglePolygonStrategy implements IGeneralizatorStrategy
{
	/**
     * Simplifie polygone en construisant une enveloppe à partir du  minimun et maximum de x et y dans cette zone 
     * @param listPoints liste des points du polygone
     * @return Polygone
     */
    public static Polygone JTSSimplifyPolygon(List<Point> listPoints)
    {
    	//Conversion des coordonnées de mon API en coordonnées de l'API JTS
    	com.vividsolutions.jts.geom.Coordinate[] coordsIn = new com.vividsolutions.jts.geom.Coordinate[listPoints.size()+1];
		int i;
		for (i = 0; i< listPoints.size(); i++) 
		{
			coordsIn[i] = new com.vividsolutions.jts.geom.Coordinate(listPoints.get(i).getX(), listPoints.get(i).getY());
		}
		coordsIn[i] = new com.vividsolutions.jts.geom.Coordinate(listPoints.get(0).getX(), listPoints.get(0).getY());		
		
		//Création de l'enveloppe autour de l'emprise des données
		com.vividsolutions.jts.geom.GeometryFactory fact = JTSFactoryFinder.getGeometryFactory();
        LinearRing linear = new com.vividsolutions.jts.geom.GeometryFactory().createLinearRing(coordsIn);
		Polygon poly = new Polygon(linear, null, fact);
		Envelope env = poly.getEnvelopeInternal();
		
		GeometryFactory geomFact = new GeometryFactory();
	    List<Point> coords = Arrays.asList((Point) geomFact.point(env.getMinX(), env.getMinY()), (Point) geomFact.point(env.getMinX(),env.getMaxY()),
	    		(Point) geomFact.point(env.getMaxX(), env.getMaxY()), (Point) geomFact.point(env.getMaxX(), env.getMinY()), (Point) geomFact.point(env.getMinX(), env.getMinY()));
	    
	    return (Polygone) geomFact.polygone(coords);
	}

    /**
     * Simplifie un polygone en construisant une enveloppe à partir du  minimun et maximum de x et y dans cette zone 
     * @param listPoints liste des points du polygone
     * @param tolerance double : pas nécessaire dans le cas du polygone
     * @return Polygone
     */
	@Override
	public List<Polygone> generalize(List<Point> listPoints, double tolerance) 
	{
		Polygone simplifiedPolygon = JTSSimplifyPolygon(listPoints);
		
	    return Arrays.asList(simplifiedPolygon);
	}
}