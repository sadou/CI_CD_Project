package ensg.tsi.tsdiakhaby.shapewritter;
/**
 * Classe abstraite permettant d'écrire des points dans un fichier shapefile
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;

import ensg.tsi.tsdiakhaby.domain.GeometryType;
import ensg.tsi.tsdiakhaby.geometries.Geometry;
import ensg.tsi.tsdiakhaby.geometries.Point;

public class PointWritter extends ShapeWritter {
	
	/**
	 * Constructeur d'un writter de Points
	 * @param filePath String chemin vers le fichier de sortie
	 * @param outputFileName String nom du fichier de sortie
	 * @param type GeometryTYpe type de géométrie à écrire (GeometryType.Point,GeometryType.LineString,GeometryType.Polygone)
	 * @param epsg int le système de projection de la couche en sortie
	 * @throws SchemaException
	 */
	public PointWritter(String filePath, String outputFileName, GeometryType type, int epsg)
			throws SchemaException {
		super(filePath, outputFileName, type, epsg);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Cette fonction permet de construire des SimpleFeature de l'API JTS à partir des géométries de mon API
	 * @param geometries List<Geometry>  liste des géométries
	 * @return List<SimpleFeature>
	 */
	@Override
	public List<SimpleFeature> buildJTSFeature(List<? extends Geometry> listGeom) 
	{
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(this.getFeatureType());
        
		GeometryBuilder geomBuild = new GeometryBuilder();
		List<SimpleFeature> features = new ArrayList<>();
         
        for (Geometry geom : listGeom) 
        {
        	Point point = (Point ) geom;
        	featureBuilder.add(geomBuild.point(point.getX(),point.getY()));
        	SimpleFeature featureBuild = featureBuilder.buildFeature(null);
            
			features.add(featureBuild); 
		}
        return features;
	}


}
