package ensg.tsi.tsdiakhaby.shapewritter;

import org.geotools.feature.SchemaException;

import ensg.tsi.tsdiakhaby.domain.GeometryType;
import ensg.tsi.tsdiakhaby.exceptions.UnknownGeometryWritterException;

public class GeometryWritterFactory 
{
	//Créateur de stratégie
		public Iwritter createWritter(String filePath, String outputFileName, GeometryType type,int epsg) throws SchemaException
		{
			if(type==GeometryType.LineString) return new LineWritter(filePath, outputFileName, type, epsg);
			else if(type ==GeometryType.Point) return new PointWritter(filePath, outputFileName, type, epsg);
			else if(type ==GeometryType.Polygon) return new PolygonWritter(filePath, outputFileName, type, epsg);
			//Si le nom de la , on lève une exception
			else throw new UnknownGeometryWritterException(type);
		}
}
