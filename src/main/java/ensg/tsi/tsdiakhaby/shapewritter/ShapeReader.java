package ensg.tsi.tsdiakhaby.shapewritter;

import java.io.File;
import java.io.IOException;

import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.store.ContentFeatureSource;
import org.geotools.feature.SchemaException;

public class ShapeReader 
{
	private String filePath;
	private String shapeFileName;
	private ShapefileDataStore dataStore;
	private boolean isClosed;
	
	public ShapeReader(String filePath, String fileName) throws SchemaException {
		this.filePath = filePath;
		this.shapeFileName = fileName;
	}
	
	public ContentFeatureSource read() throws IOException
	{
		File file = new File(this.filePath+"/"+this.shapeFileName+".shp");
        this.dataStore = new ShapefileDataStore(file.toURI().toURL());
        this.isClosed = false;
        return  this.dataStore.getFeatureSource();
	}
	
	
	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getShapeFileName() {
		return shapeFileName;
	}

	public void setShapeFileName(String shapeFileName) {
		this.shapeFileName = shapeFileName;
	}

	public ShapefileDataStore getDataStore() {
		return dataStore;
	}

	public void setDataStore(ShapefileDataStore dataStore) {
		this.dataStore = dataStore;
	}

	public boolean isClosed() {
		return isClosed;
	}

	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}

	public void close()
	{
		this.dataStore.dispose();
		this.isClosed = true;
	}
}
