package ensg.tsi.tsdiakhaby.shapewritter;
/**
 * Classe permettant d'écrire des polygones dans un fichier shapefile
 * @author tsdiakhaby
 * @version 1.1
 */
import java.util.ArrayList;
import java.util.List;

import org.geotools.feature.SchemaException;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Polygon;

import ensg.tsi.tsdiakhaby.domain.GeometryType;
import ensg.tsi.tsdiakhaby.geometries.Geometry;

public class PolygonWritter extends ShapeWritter 
{
	
	/**
	 * Constructeur d'un writter de Polygones
	 * @param filePath String chemin vers le fichier de sortie
	 * @param outputFileName String nom du fichier de sortie
	 * @param type GeometryTYpe type de géométrie à écrire (GeometryType.Point,GeometryType.LineString,GeometryType.Polygone)
	 * @param epsg int le système de projection de la couche en sortie
	 * @throws SchemaException
	 */
	public PolygonWritter(String filePath, String outputFileName, GeometryType type, int epsg)
			throws SchemaException {
		super(filePath, outputFileName, type, epsg);
		// TODO Auto-generated constructor stub
	}
	
	
	/**
     * Simplifie polygone en construisant une enveloppe à partir du  minimun et maximum de x et y dans cette zone 
     * @param listPoints liste des points du polygone
     * @return Polygone
     */
	public static Polygon generatePolygon(Polygon poly){
		Envelope env = poly.getEnvelopeInternal();
        Coordinate[] coords = {new Coordinate(env.getMinX(), env.getMinY()), new Coordinate(env.getMinX(),env.getMaxY()),
        		new Coordinate(env.getMaxX(), env.getMaxY()), new Coordinate(env.getMaxX(), env.getMinY()), new Coordinate(env.getMinX(), env.getMinY())};
        
        GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
        return geometryFactory.createPolygon(coords);
		}

	
	/**
	 * Cette fonction permet de construire des SimpleFeature de l'API JTS à partir des géométries de mon API
	 * @param geometries List<Geometry>  liste des géométries
	 * @return List<SimpleFeature>
	 */
	@Override
	public List<SimpleFeature> buildJTSFeature(List<? extends Geometry> listGeom) 
	{
		SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(this.getFeatureType());
        
		List<SimpleFeature> features = new ArrayList<>();
         
        for (Geometry geom : listGeom) 
        {
        	GeometryFactory fact = new GeometryFactory();
        	com.vividsolutions.jts.geom.Coordinate[] coords = new com.vividsolutions.jts.geom.Coordinate[geom.getCoordinate().length];
        	
        	int i;
        	for (i = 0; i< geom.getCoordinate().length; i++) 
			{
				coords[i] = new com.vividsolutions.jts.geom.Coordinate(geom.getCoordinate()[i].getX(), geom.getCoordinate()[i].getY());
			}
			
			//Ecriture des fichiers dans un format JTS
			LinearRing linear = new GeometryFactory().createLinearRing(coords);
			featureBuilder.add(generatePolygon(new Polygon(linear, null, fact)));
			
			SimpleFeature featureBuild = featureBuilder.buildFeature(null);
			features.add(featureBuild); 
		}
        return features;
	}
}
