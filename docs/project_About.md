# API de généralisation cartographique
La généralisation est un processus cartographique consistant à réduire le niveau de détail
de primitives géométriques (d’une manière générale, de l’information (sémantique, géographique,
toponymique, etc.)) afin d’obtenir une représentation adaptée à un thème donné et une échelle
donnée.
Objectifs :
- N’importe quelle géométrie en entrée
- Implémentations :
Polylignes : Douglas Peucker ; lissage gaussien
Points : grille régulière
Polygones : que des angles droits en sortie
  
  * Maven Build
  * JUnit-tested
    * 88,2% de lignes couvertes


# Installation de l'API avec maven
Télécharger le fichier .jar et mettez-le quelque part dans votre système (de préférence à côté de votre projet)
Puis ajoutez la dépendance maven suivante.
```xml
<dependency>
    <groupId>ensg.tsi.dei</groupId>
    <artifactId>tsdiakhaby</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <scope>system</scope>
    <systemPath>"Path to Jarfile"</systemPath>
</dependency>
```
# Création de géométrie #
Pour toute instanciation de géométrie il faut passer par la classe géométrie factory **GeometryFactory**
```java
   //Instanciation d'une GeometrieFactory
     GeometrieFactory factory = new GeometrieFactory()
   
   //Instanciation d'un Point
     Point point = (Point) factory.point(12.5,13.5);
  
   //Instanciation d'une ligne
     Line ligne = (Line) factory.line(); //instanciation d'une ligne vide
   
   //Instanciation d'un polygone
     Polygone poly = (Polygone) factory.polygone(); //instanciation d'un polygone vide
```
# Généralisation d'un ensemble de points #
Ici, nous utiliserons l'algorithme basé sur une grille régulière
```java
    // créer un ensemble de point constituant le nuage de points
    Point[] allPoints = ...
    double tolerance = ... //Distance à partir de la quelle on supprime un point du nuage 

    //Définir une stratégie pour la généralisation avec la classe **StrategyFactory**
     StrategyFactory factory = new StrategyFactory() //Créateur de stratégie
     RegularGridStrategy gridStrategy = (RegularGridStrategy) factory.createStrategy("regularGrid");
     List<Point> simplifiedPoint = gridStrategy.generalize(allpoints, tolerance); // généralisation par une grille régulière    
```

# Généralisation d'une ligne #
Ici, nous utiliserons l'algorithme basé sur la méthode de  [Douglas Peucker](https://fr.wikipedia.org/wiki/Algorithme_de_Douglas-Peucker)
```java
    // créer un ensemble de point constituant la ligne
    Point[] allPoints = ...
    double tolerance = ... //Distance à partir de la quelle on supprime un point de ligne 
    //Exécution de l'algorithme de Douglas Peucker sur les points
    
     //Définir une stratégie pour la généralisation avec la classe **StrategyFactory**
     StrategyFactory factory = new StrategyFactory() //Créateur de stratégie
     DouglasPeuckerStrategy douglasStrategy = (DouglasPeuckerStrategy) factory.createStrategy("douglasPeucker"); // généralisation par la méthode de Douglas Peucker 
     List<Line> lineSimplified = douglasStrategy.generalize(allpoints, tolerance);
    
```
# Généralisation  d'un polygone #
La généralisation d'un polygone dans notre API est basé sur la méthode la génération du rectangle englobant le polygone ne générant
que des angles droits en sortie.
```java
    // créer un ensemble de point constituant la ligne
    Point[] allPoints = ...
    double tolerance = ... //Cette valeur bien qu'elle ne soit assez nécessaire, elle doit être définie. 
    //Exécution de l'algorithme de Douglas Peucker sur les points
    
     //Définir une stratégie pour la généralisation avec la classe **StrategyFactory**
     StrategyFactory factory = new StrategyFactory() //Créateur de stratégie
     RightAnglePolygonStrategy rightAngleStrategy = (RightAnglePolygonStrategy) factory.createStrategy("rightAngleStrategy"); // généralisation par la méthode du rectange englobant
     List<Polygone> lineSimplified = douglasStrategy.generalize(allpoints, tolerance);//En sortie une List contenant un seul polygone
    
```

# Export du résultat en shapefile #
L'API permet d'exporter les résultats des différents algorithmes dans un shapefile. 
En effet, le résultat de chacun des algorithmes est une liste de géométrie
Pour exporter une liste de géométrie en shapefile, il faudra créer une instance de **GeometryWritterFactory**
```java
    // créer d'une instance de GeometryWritterFactory
    GeometryWritterFactory geomWFactory = new GeometryWritterFactory();
    // Création d'une instance pour créer des points dans un shapefile
       PointWritter pWriter = (PointWritter) geomWFactory.createWritter(inputFilePath, fileName, GeometryType.Point, 2154);
        
    //Ecriture dans le fichier 
	List<Point> results = ...
	pWriter.write(results);

```

**Développé par DIAKHABY Thierno Sadou**
**ENSG TSI 2017**


